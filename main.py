import aiogram
from create_bot import dp
from handlers import user_handlers

user_handlers.register_handlers(dp)

aiogram.utils.executor.start_polling(dp, skip_updates=True)
# aiogram.utils.executor.start_webhook(dp, skip_updates=True)