import aiogram
from create_bot import bot
from keyboards import keyboard
import utils
from PIL import Image
from io import BytesIO

async def command_start(message: aiogram.types.Message):
    """Обработчик команды /start"""
    await bot.send_message(
        message.from_user.id,
        "Здравствуйте! Я умею выдавать на любое сообщение шифр цезаря или "
        "ответ в виде картинки по Вашему желанию.",
        reply_markup=keyboard.kb,
    )

async def command_help(message: aiogram.types.Message):
    """Обработчик команды /help"""
    help_text = (
        "* Режим: шифр цезаря — это вид шифра подстановки, в котором "
        "каждый символ в открытом тексте заменяется символом, "
        "находящимся на некотором постоянном числе позиций "
        "левее или правее него в алфавите. Например, в шифре "
        "со сдвигом вправо на 3, А была бы заменена на Г, Б "
        "станет Д, и так далее.\n\n"
        "* Режим: картинки — на любое Ваше сообщение будет выдаваться "
        "случайная картинка.\n\n"
        "* Режим: по умолчанию — на любое Ваше сообщение будет выдаваться "
        "полученное ботом сообщение."
    )
    await bot.send_message(message.from_user.id, help_text)

async def command_caesar_cipher(message: aiogram.types.Message):
    """Обработчик команды /caesar_cipher"""
    utils.mode_bot.mode = 'caesar_cipher'
    await bot.send_message(message.from_user.id, "Режим: шифр цезаря")

async def command_images(message: aiogram.types.Message):
    """Обработчик команды /image"""
    utils.mode_bot.mode = 'image'
    await bot.send_message(message.from_user.id, "Режим: картинки")

async def command_default(message: aiogram.types.Message):
    """Обработчик команды /reset"""
    utils.mode_bot.mode = 'default'
    await bot.send_message(message.from_user.id, "Режим: по умолчанию")

async def send(message: aiogram.types.Message):
    """Обработчик всех остальных сообщений"""
    returned = utils.mode_bot.mode_bot(message.text)
    if isinstance(returned, Image.Image) and utils.mode_bot.mode == 'image':
        with BytesIO() as buffer:
            returned.save(buffer, format='PNG')
            await bot.send_photo(message.from_user.id, buffer.getvalue())
    else:
        await bot.send_message(message.from_user.id, returned)

def register_handlers(dp: aiogram.dispatcher.Dispatcher):
    """Регистрация всех обработчиков команд"""
    dp.register_message_handler(command_start, commands=['start'])
    dp.register_message_handler(command_help, commands=['help'])
    dp.register_message_handler(command_caesar_cipher, commands=['caesar_cipher'])
    dp.register_message_handler(command_images, commands=['image'])
    dp.register_message_handler(command_default, commands=['reset'])
    dp.register_message_handler(send)