import aiogram

START_BUTTON = "/start"
HELP_BUTTON = "/help"
CAESAR_CIPHER_BUTTON = "/caesar_cipher"
IMAGE_BUTTON = "/image"
RESET_BUTTON = "/reset"

kb = aiogram.types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
kb.add(
    aiogram.types.KeyboardButton(START_BUTTON),
    aiogram.types.KeyboardButton(HELP_BUTTON),
    aiogram.types.KeyboardButton(CAESAR_CIPHER_BUTTON),
    aiogram.types.KeyboardButton(IMAGE_BUTTON),
    aiogram.types.KeyboardButton(RESET_BUTTON)
)

