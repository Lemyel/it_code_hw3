import random
import requests
from io import BytesIO
from PIL import Image

mode = 'default'

def mode_caesar_cipher(text : str):
    alphabet_sizes = {'en': 26, 'ru': 33}
    alphabet = {'en': 'abcdefghijklmnopqrstuvwxyz', 'ru': 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'}
    lang = 'en' if any(char.lower() in alphabet['en'] for char in text) else 'ru'
    shift = random.randint(-10, 10)  # выбираем случайное число от -10 до 10
    result = []

    for char in text:
        if char.lower() in alphabet[lang]:
            num = ord(char.lower()) - ord(alphabet[lang][0])
            num = (num + shift) % alphabet_sizes[lang]
            char_new = chr(num + ord(alphabet[lang][0]))
        else:
            char_new = char
        result.append(char_new.upper() if char.isupper() else char_new)
        
    return ''.join(result), shift


def mode_image(text):
    while True:
        response = requests.get("https://random.dog/woof.json")
        json_data = response.json()
        image_url = json_data["url"]

        if image_url.endswith(('.jpg', '.jpeg', '.png', '.gif')):
            response = requests.get(image_url)
            image = Image.open(BytesIO(response.content))

            width, height = image.size
            new_size = (int(width * 0.5), int(height * 0.5))
            image = image.resize(new_size)

            return image

def mode_bot(text):
    if mode == 'default':
        return text
    elif mode == 'caesar_cipher':
        caesar = mode_caesar_cipher(text)
        return f'значение сдвига: {caesar[1]}.\n{caesar[0]}'
    elif mode == 'image':
        return mode_image(text)
